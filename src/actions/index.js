import { CONNECTION, DECONNECTION } from "../actions/types";

export const connection = (token) => ({
  type: CONNECTION,
  token:token
});

export const deconnection = () => ({
  type: DECONNECTION,
});
