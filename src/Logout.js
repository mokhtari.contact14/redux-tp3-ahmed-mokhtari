import React from 'react';
import './App.css';

export default class Logout extends React.Component{

    constructor(props) {
        super(props);
        this.myOnClick = this.myOnClick.bind(this);
      }

    myOnClick() {

        fetch('https://los.ling.fr/logout', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
        .then(result => {
                const elem = document.querySelector('.out__btn');
                elem.innerHTML = "";
                alert("Good Bye :) !");
        })
    }
      
    render(){
        return(
            <div className="row out__btn">
                <button onClick={this.myOnClick}>Se déconnecter</button>
            </div>);
    }
   
}