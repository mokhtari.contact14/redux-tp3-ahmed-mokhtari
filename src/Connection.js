import React from 'react';
import './App.css';
import { Form, Field } from "react-final-form"
import { connection, deconnection } from './actions';
import { connect } from 'react-redux';
import Logout from './Logout'


const handleConnection = (formRes, loginSucceeded) => {

    fetch('https://los.ling.fr/login', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formRes)
    })
    .then(reponse => reponse.json())
    .then(result => {
        console.log(result);
        loginSucceeded(result.token)

        const body = document.querySelector('.main__login');
        body.innerHTML = "";
        document.querySelector('.__after').style.display = "block";
    })
}


//composant react
const Connection = ({loginSucceeded, stateToken, logOut}) =>
    (
        <div className="container-fluid text-center main__login">
            <div className="__after" style={{display: "none"}}><Logout log={logOut} val={stateToken}></Logout></div>
            <div className="row">
                <h1 className="title">Connexion</h1>
                <div className="col-md-5"></div>
                <Form
                    onSubmit={(formRes) => handleConnection(formRes, loginSucceeded)}
                    render={({ handleSubmit }) => (
                        <div className="row">
                        <div className="col-md-5">
                        <form className="form" onSubmit={handleSubmit}>
                            <div>
                                <label className="form-label">Pseudo</label>
                                <Field
                                    name="pseudo"
                                    component="input"
                                    className="form-control"
                                    placeholder="Pseudo"
                                    type="text"
                                    required
                                />
                            </div>
                            <div>
                                <label className="form-label">Mot de passe</label>
                                <Field
                                    name="password"
                                    component="input"
                                    className="form-control"
                                    placeholder="Mot de passe"
                                    type="password"
                                    required
                                />
                            </div>
                            <button className="btn btn-info mt-4" type="submit">Se connecter</button>
                        </form>
                        </div>
                        </div>
                    )} />
                <div className="col-md-5"></div>
            </div>  
        </div>
    );

const mapDispatchToProps = (dispatch) => ({
    loginSucceeded: (token) => {dispatch(connection(token))},
    logOut: () => {dispatch(deconnection())}
})

const mapStateToProps = (state) =>({
    stateToken : state.token
})

export default connect(mapStateToProps, mapDispatchToProps)(Connection);