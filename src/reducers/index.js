import { CONNECTION, DECONNECTION } from "../actions/types";

const initState = {token : null, connected : false};
const auth = (state = initState, action) => {
  switch (action.type) {
    case CONNECTION:
        return {
            token: action.token,
            connected: true
          };
    case DECONNECTION:
          return {
            token: null,
            connected: false
          };
    default:
      return state
  }
}

export default auth;