import './App.css';
import Header from './Header';
import Connection from './Connection';
import {BrowserRouter as Router , Route } from "react-router-dom";

function connection(){
    return <Connection />
}

function App() {
  return (
    <div className="App">
      <Router>
        <div>
        <Header />
        <Route path="/connection/" component={connection}/>
        </div>
      </Router>
    </div>
  );
}

export default App;
